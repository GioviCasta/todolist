let todoList =[
    {title : 'Fare Lezione' , description : 'Ricordati di fare lezione sul dom' , expiration_date : '2022-02-22T09:30' , completed : false},
    {title : 'Fare La Spesa' , description : 'Ricordati di fare la spesa da Carletto' , expiration_date : '2022-02-25T18:30' , completed : false},
];

let editTodoIndex 

//  SEZIONE ELEMENTI DOM
let todoWrapper = document.querySelector('#todoWrapper');
let todoCompletedWrapper = document.querySelector('#todoCompletedWrapper')
let titleInput = document.querySelector('#titleInput');
let descriptionInput = document.querySelector('#descriptionInput')
let dateInput = document.querySelector('#dateInput')
let previewTitle = document.querySelector('#previewTitle');
let previewDescription = document.querySelector('#previewDescription');
let previewExpireDate = document.querySelector('#previewExpireDate');
let createBtn = document.querySelector('#createBtn');
let resetBtn = document.querySelector('#resetBtn');
let notifyDiv = document.querySelector('.notify')
let notifyMessage = document.querySelector('#notify-message')
let creationBtnWrapper  = document.querySelector('#creationBtnWrapper')
let editBtnWrapper  = document.querySelector('#editBtnWrapper')
let saveEditBtn = document.querySelector('#saveEditBtn')
let unduEditBtn = document.querySelector('#unduEditBtn')



// SEZIONE FUNZIONI
function showTodoList() {
    todoWrapper.innerHTML = '';
    todoCompletedWrapper.innerHTML = '';
    todoList.forEach( (todo , index ) => {
        let div = document.createElement('div');
        div.classList.add( 'col-12' ,'col-md-4' ,'col-xl-3' , 'my-2')
        div.innerHTML= `
        <div class="card text-center">
            <div class="card-header  ${todo.completed ? 'bg-success' : ''}">
                <h6 class="card-title  ${todo.completed ? 'text-white fw-bold' : ''}">
                ${todo.title} 
                ${todo.completed ? '<i class="fa-solid fa-circle-check"></i>' : ''} 
                </h6>
            </div>
            <div class="card-body">
                <p class="card-text">${todo.description}</p>
                <div class="d-flex flex-column">
                    <button data-complete="${index}" class="btn btn-success my-1 btn-complete" ${todo.completed ? 'disabled' : ''}>Completa</button>
                    <button data-modify="${index}" class="btn btn-primary my-1 btn-modify"${todo.completed ? 'disabled' : ''}>Modifica</button>
                    <button data-delete="${index}" class="btn btn-danger my-1 btn-delete">Elimina</button>
                </div>
            </div>
            <div class="card-footer text-muted fw-bold">
               ${dateFormat(todo.expiration_date)}
            </div>
        </div>
        `
        if (todo.completed) {
            todoCompletedWrapper.appendChild(div);
        } else {
            todoWrapper.appendChild(div);
        }
    });

    
    attachDeleteEvent();
    modifyElement();   
    attachCompleteEvent();
}

function attachCompleteEvent() {
    let completeBtns = document.querySelectorAll('.btn-complete');

    completeBtns.forEach(btn => {
        btn.addEventListener( 'click' , () =>{
            let todoIndex = btn.getAttribute('data-complete');
            
            todoList[todoIndex].completed = true;
            
            showTodoList();

        })
    })
}
function attachDeleteEvent() {
    let deleteBtns = document.querySelectorAll('.btn-delete');

    deleteBtns.forEach(btn => {
        btn.addEventListener( 'click' , () =>{
            let todoIndex = btn.getAttribute('data-delete');
            
            todoList.splice(todoIndex, 1);
            showTodoList();
        })
    })
}

function modifyElement() {
    let modifyBtns = document.querySelectorAll('.btn-modify');
    modifyBtns.forEach(btn => {
        btn.addEventListener('click' , ()=>{
            let todoIndex = btn.getAttribute('data-modify');

            //attivo modalita' modifica
            activeEditMode();   
            //indice elemento da modificare
            editTodoIndex = todoIndex;
            //popolo gli i dati negli input
            let todo = todoList[todoIndex];
            titleInput.value = todo.title ;
            descriptionInput.value = todo.description;
            dateInput.value= todo.expiration_date;
            //popolo i dati nella preview
            previewTitle.innerHTML = todo.title;
            previewDescription.innerHTML = todo.description;
            previewExpireDate.innerHTML = dateFormat(todo.expiration_date) ; 
            
        })
    })
}


function dateFormat(date){
    date = new Date(date);
    return date.toLocaleDateString( 'it-IT' , { year: 'numeric', month: 'long', day: 'numeric', hour :'numeric', minute: 'numeric'});
}

function resetImput() {
    titleInput.value = ''
    descriptionInput.value = ''
    dateInput.value = ''
    previewTitle.innerHTML = ''
    previewDescription.innerHTML = '' 
    previewExpireDate.innerHTML = ''    
}

function disablePastDate () {
    let now = new Date().toISOString()
    let T = now.search('T');
    now = now.substring( 0 , T + 6  )
    dateInput.setAttribute('min' , now )
}

function notify( type , message , delay) {

    notifyMessage.innerHTML = message;

    if(type == 'info'){
        notifyDiv.classList.add('active');
    }else if (type == 'error'){
        notifyDiv.classList.add('active');
        notifyDiv.classList.add('notify-error');
    } else if (type == 'success') {
        notifyDiv.classList.add('active');
        notifyDiv.classList.add('notify-success');
    }

    setTimeout( () => {
        notifyDiv.classList.remove('active');
        notifyDiv.classList.remove('notify-error');
        notifyDiv.classList.remove('notify-success');
    }, delay);
}

function activeInsertMode() {
    
    editBtnWrapper.classList.add('d-none');
    creationBtnWrapper.classList.remove('d-none');
}

function activeEditMode() {
    
    creationBtnWrapper.classList.add('d-none');
    editBtnWrapper.classList.remove('d-none');
}    
    
function validateForm(title , description , date) {
   let errorTitle = false; 
   let errorDescription = false;
   let errorDate = false;
    
   let errorTitleMessage = '';
   let errorDescriptionMessage = '';
   let errorDateMessage = '';

    if( !title || title.length < 3 ){
        errorTitleMessage = 'Inserire titolo';
        errorTitle = true;
    }

    if ( !description ) {
        errorDescriptionMessage = 'Inserire Descrizione'
        errorDescription = true;
    }

    let expire_date = Date.parse(date);
    let now = Date.now();
   
    
    if( !date){
        errorDateMessage = 'Inserire Data';
        errorDate = true;
    }  

    if(!errorTitle && !errorDescription && !errorDate){
        notify('success' , ` ToDo inserito! `, 2000)
        return true
    } else {
        notify('error' , ` ${errorTitleMessage} <br> ${errorDescriptionMessage} <br> ${errorDateMessage} <br> `, 5000)

    }
    
}

//SEZIONE EVENTI
showTodoList()
disablePastDate()

titleInput.addEventListener('input' , () => {
    previewTitle.innerHTML = titleInput.value;
})


descriptionInput.addEventListener('input' , () => {
    previewDescription.innerHTML = descriptionInput.value;
})

dateInput.addEventListener('change' , () => {
    previewExpireDate.innerHTML = dateFormat(dateInput.value)
})


createBtn.addEventListener('click' , () =>{
    let title = titleInput.value;
    let description = descriptionInput.value;
    let expire_date = dateInput.value;

    if (validateForm( title , description, expire_date)){
        todoList.push({title : title, description : description, expire_date : expire_date , completed : false});
        showTodoList();
        resetImput();

    }
    
})

saveEditBtn.addEventListener('click' , () =>{
    console.log('ciao');
    let title = titleInput.value;
    let description = descriptionInput.value;
    let expire_date = dateInput.value;
    
    if (validateForm( title , description, expire_date)){
        todoList[editTodoIndex] = {title : title, description : description, expire_date : expire_date , completed: todoList[editTodoIndex].completed };
        showTodoList();
        resetImput();
        activeInsertMode();

    }
});

unduEditBtn.addEventListener('click' , () =>{
    activeInsertMode();
    resetImput();
})

resetBtn.addEventListener('click' , () =>{
    resetImput()
})


fetch('./data.json')
.then(resp => resp.json())
.then(data => {
    todoList= data;
    showTodoList();

})